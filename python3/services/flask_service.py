import flask
import optparse
import time
import os
import tornado.wsgi
import tornado.httpserver
import tornado.ioloop

APP_NAME = "flask_service"
app = flask.Flask(APP_NAME)
app.config['SECRET_KEY'] = os.getenv('SECRET_KEY') or os.urandom(16)


def start_tornado(app, port):
    try:
        max_buffer_size = 1024 * 1024 * 20  # 20M
        http_server = tornado.httpserver.HTTPServer(
            tornado.wsgi.WSGIContainer(app),
            max_buffer_size=max_buffer_size,
        )

        http_server.listen(port)
        tornado.ioloop.IOLoop.instance().start()
    except Exception as e:
        print("{}".format(e))

def stop_tornado():
    try:
        tornado.ioloop.IOLoop.instance().stop()
    except Exception as e:
        print("{}".format(e))



@app.route("/api/python/flask/hello", methods=['GET'])
def hello():
    return {"message": "Flask, Hello World"}


@app.route("/api/python/flask/hello01", methods=['GET'])
def hello01():
    time.sleep(0.1)
    return {"message": "Flask, sleep 0.1s before return Hello World"}

@app.route("/api/python/flask/hello001", methods=['GET'])
def hello001():
    time.sleep(0.01)
    return {"message": "Flask, sleep 0.01s before return Hello World"}


@app.route("/api/python/flask/hello0001", methods=['GET'])
def hello0001():
    time.sleep(0.001)
    return {"message": "Flask, sleep 0.001s before return Hello World"}




if __name__ == '__main__':
    try:
        parser = optparse.OptionParser()
        parser.add_option('--port', type='int', default=5001)
        
        opts, args = parser.parse_args()
        port = opts.port

        print("Starting Tornado server on port {}".format(port))
        start_tornado(app, port=port)
    except Exception as e:
        print("Main error: {} ".format(e))
