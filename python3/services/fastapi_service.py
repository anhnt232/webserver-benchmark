from fastapi import FastAPI

import time

app = FastAPI()

@app.get("/api/python/fastapi/hello")
async def root():
    return {"message": "fastAPI, Hello World"}

@app.get("/api/python/fastapi/hello01")
async def root():
    time.sleep(0.1)
    return {"message": "fastAPI, sleep 0.1s before return Hello World"}

@app.get("/api/python/fastapi/hello001")
async def root():
    time.sleep(0.01)
    return {"message": "fastAPI, sleep 0.01s before return Hello World"}

@app.get("/api/python/fastapi/hello0001")
async def root():
    time.sleep(0.001)
    return {"message": "fastAPI, sleep 0.001s before return Hello World"}



