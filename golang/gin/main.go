package main

import "github.com/gin-gonic/gin"

func main() {
	r := gin.New()
	r.GET("/api/golang/gin/hello", func(c *gin.Context) {
		c.String(200, c.ClientIP())
	})
	r.Run() // listen and serve on 0.0.0.0:8080
}
