package main

import (
	"flag"
	"fmt"
	"log"
	"strings"

	"github.com/valyala/fasthttp"
)

var (
	addr     = flag.String("addr", ":50003", "TCP address to listen to")
	compress = flag.Bool("compress", false, "Whether to enable transparent response compression")
)

func main() {
	flag.Parse()

	h := requestHandler
	if err := fasthttp.ListenAndServe(*addr, h); err != nil {
		log.Fatalf("Error in ListenAndServe: %s", err)
	}
}

func requestHandler(ctx *fasthttp.RequestCtx) {
	ctx.SetContentType("text/plain; charset=utf8")
	xForwardedHeader := string(ctx.Request.Header.Peek("X-Forwarded-For")[:])
	if xForwardedHeader == "" {
		fmt.Fprintf(ctx, "%v", ctx.RemoteIP())
		return
	}
	xForwardedSplit := strings.Split(xForwardedHeader, ",")
	if len(xForwardedSplit) > 0 {
		fmt.Fprintf(ctx, "%v", xForwardedSplit[0])
	} else {
		fmt.Fprintf(ctx, "%v", ctx.RemoteIP())
	}
}
